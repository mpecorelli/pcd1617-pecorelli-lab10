package pcd.ass03;

import io.reactivex.Flowable;

public interface TemperatureSensor {
	
	double getCurrentValue();	
	
}
