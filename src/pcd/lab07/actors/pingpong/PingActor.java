package pcd.lab07.actors.pingpong;

import akka.actor.*;

public class PingActor extends UntypedActor {

	  public void preStart() {
		  final ActorRef ponger = getContext().actorOf(Props.create(PongActor.class), "ponger");
		  ponger.tell(new PingMsg(0), getSelf());
	  }
	
	  @Override
	  public void onReceive(Object msg) {
		  PongMsg mess = (PongMsg) msg;
		  System.out.println("PONG received: "+  mess.getValue());
		  getSender().tell(new PingMsg( mess.getValue() + 1), getSelf());
	  }
	
}
