package pcd.lab07.actors.macrostepsem;

import akka.actor.UntypedActor;

public class StoppableActor extends UntypedActor {
	private boolean stopped;
	
	public void onReceive(Object msg) {
		if (msg instanceof PrintMsg) {
			stopped = false;
			this.getSelf().tell(new PrintNextMsg(0, ((PrintMsg)msg).getNum()), getSelf());
		} else if (msg instanceof PrintNextMsg) {
			int n = ((PrintNextMsg)msg).getNum();
			int max = ((PrintNextMsg)msg).getMax();
			System.out.println(n);
			if (n < max && !stopped){
				this.getSelf().tell(new PrintNextMsg(n + 1, max), getSelf());
			}
		} else if (msg instanceof StopMsg) {
			System.out.println("stopped!");
			stopped = true;
		} else {
			unhandled(msg);
		}
	}
}
