package pcd.lab10;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Client2 {

    private Client2() {}

    public static void main(String[] args) {

        String host = (args.length < 1) ? null : args[0];
        try {
            Registry registry = LocateRegistry.getRegistry(host);
            HelloService obj = (HelloService) registry.lookup("helloObj2");
            
            MyClass arg = new MyClassImpl(300); 
            System.out.println("before: >> "+arg.get());

            UnicastRemoteObject.exportObject(arg, 0);

            String response = obj.sayHello(arg);
            System.out.println("response: " + response);
            System.out.println("after: >> "+arg.get());
            
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }
}