package pcd.lab10;

import java.rmi.*;

public interface MyClass extends Remote {

	int get() throws RemoteException;

	void update(int c) throws RemoteException;
	
}