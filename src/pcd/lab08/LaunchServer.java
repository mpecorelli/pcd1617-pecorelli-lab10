package pcd.lab08;

import io.vertx.core.Vertx;

public class LaunchServer {

	public static void main(String[] args) {
		Vertx  vertx = Vertx.vertx();
		Server myVerticle = new Server();
		vertx.deployVerticle(myVerticle);
	}

}
