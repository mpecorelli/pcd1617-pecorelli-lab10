package pcd.ass02.ex1.sol;

import java.util.Random;

import pcd.ass02.ex1.*;

public class Player extends Thread {

	private OracleInterface oracle;
	private int myId;
	
	public Player(int id, OracleInterface oracle){
		myId = id;
		this.oracle = oracle;
	}
	
	public void run(){
		long min = 0;
		long max = Long.MAX_VALUE;
		Random gen = new Random();
		boolean gameFinished = false;
		boolean won = false;
		long value = -1;
		while (!gameFinished){
			value = min + (max-min > 0 ? Math.abs(gen.nextLong()) % (max-min) : max);
			try {				
				log("Trying "+value+"...");
				Result res = oracle.tryToGuess(myId, value);
				if (res.found()){
					gameFinished = true;
					won = true;
				} else if (res.isGreater()){
					min = value;
				} else {
					max = value;
				}
			} catch (GameFinishedException ex){
				gameFinished = true;
			}
		}
		
		if (won){
			log("WON! The number is "+value);
		} else {
			log("SOB!");
		}
	}
	
	protected void log(String msg){	
		synchronized (System.out){
			System.out.println("[ PLAYER-"+myId+"] "+msg);
		}
	}
}
