package pcd.ass02.ex2.sol;

import java.util.concurrent.Callable;

import pcd.ass02.ex2.*;


public class ComputeMandelbrotRegionTask implements Callable<Void>{

	private MandelbrotSetImage image;
	private int xStart, yStart;
	private int delta;
	private int nPoints;
	private int nIterMax;
	
	public ComputeMandelbrotRegionTask(MandelbrotSetImage image, int xStart, int yStart, int delta, int nPoints, int nIterMax){
		this.image = image;
		this.xStart = xStart;
		this.delta = delta;
		this.nPoints = nPoints;
		this.nIterMax = nIterMax;
	}

	@Override
	public Void call() throws Exception {
		int y = yStart;
		int x = xStart;
		int oldx = 0;
		int np = 0;
		log("Computing points from ("+x+","+y+") every "+delta);
		while (np < nPoints /* && y < image.getHeight() */){
			image.compute(x,y,nIterMax);
			oldx = x;
			x = (x + delta) % image.getWidth();
			if (x < oldx){
				y++;
			}
			np++;
		}
		log("Done.");
		return null;
	}

	private void log(String msg){
		// System.out.println("[TASK-"+this+"] "+msg);
	}
	
}
