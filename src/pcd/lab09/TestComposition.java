package pcd.lab09;

import java.util.*;

import io.reactivex.Observable;

public class TestComposition {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> words = Arrays.asList(
				 "the",
				 "quick",
				 "brown",
				 "fox",
				 "jumped",
				 "over",
				 "the",
				 "lazy",
				 "dog"
				);

		Observable<String> o1 = Observable.fromIterable(words);
		Observable<Integer> o2 = Observable.range(1, 5);
		Observable<String> o3 = o1.zipWith(o2, (string, count) -> String.format("%2d. %s", count, string));

		/*
		Observable.fromIterable(words)
				.zipWith(Observable.range(1, 5),
						 (string, count) -> String.format("%2d. %s", count, string))
				.subscribe(System.out::println);
		*/
		o3.subscribe(System.out::println);
	}

}
